@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
<div class="container">
  <div class="row">

    @if(!empty($feeds) && !empty($files))

      <section class="col-md-6">
        
        @include('dashboard.fb')
      </section>

      <section class="col-md-6">
        
        @include('dashboard.drive')
      </section>

    @elseif((!empty($feeds) && empty($files)))

      <section class="col-md-6">
        @include('dashboard.fb')
      </section>

      <section class="col-md-6">
        <h4>Kindly login with google to access your drive</h4>
        <div class="col-sm-6 ">
          <a href="auth/google" class="btn btn-danger btn-block"> Login with Google </a>
        </div>
      </section>
    @elseif(!empty($files) && empty($feeds))

      <section class="col-md-6">
        <h4>Kindly login with facebook to access your drive</h4>
        <div class="col-sm-6 ">
          <a href="auth/facebook" class="btn btn-primary btn-block"> Login with Facebook </a>
        </div>
      </section>

      <section class="col-md-6">
        @include('dashboard.drive')
      </section>
    @endif
  </div>
</div>

<script src="assets/js/dashboard.js"></script>
@endsection