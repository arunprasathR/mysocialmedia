<div class="panel panel-primary">
    <div class="panel-heading text-center">Your Recent Posts</div>

    <div class="panel-body">
        <table class="table table-hover table-inverse table-striped">
            <thead>
                <tr>
                  <th>#</th>
                  <th>From</th>
                  <th>Posts</th>
                  <th>On</th>
                  <th>View</th>
                </tr>
            </thead>
            <tbody>
                @foreach($feeds as $index => $feed)
                    <tr>
                        <th scope="row">{{ $index + 1 }}</th>
                        <td class="col-md-2">
                            @if(!empty($feed->name)) 
                                {{ $feed->name }}
                            @endif
                        </td>
                        <td class="col-md-6">
                            <h4>{{ $feed->story }}</h4>
                            @if(!empty($feed->full_picture))    
                                <img src="{{ $feed->full_picture }}"  width="100%" height="40%" alt="">
                            @endif
                            @if(!empty($feed->message)) 
                                <h5>{{ $feed->message }}</h5>
                            @endif
                        </td>
                        <td class="col-md-2">{{ Carbon\Carbon::parse($feed->created_time)->format('d-m-Y h:i') }}</td>
                        <td>
                            @if(!empty($feed->link))
                                <a href="{{ $feed->link }}"  target="_blank">view</a>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

