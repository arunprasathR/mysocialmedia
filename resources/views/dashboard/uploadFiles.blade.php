<div class="panel panel-info">
    <div class="panel-heading text-center">Upload Files</div>

    <div class="panel-body">
        @if(!empty($message))

            <h3>{{ $message }}</h3>
        @endif
        {{ Form::open(array('url' => '/uploadFiles', 'class' => 'form-horizontal col-sm-10 col-sm-offset-1', 'files' => true )) }}
            {{ csrf_field() }}

             <div class="form-group">
                {{ Form::label('file', 'Choose your file : ') }}
                {{ Form::file('file', array('class' => 'filestyle form-control', 'data-buttonName' => 'btn-primary')) }}
                @if ($errors->has('file'))
                    <span class="help-block">
                        <strong>{{ $errors->first('file') }}</strong>
                    </span>
                @endif
            </div>
            
            <div class="form-group">
                {{ Form::label('description', 'Description') }}
                {{ Form::textarea('description', null, array('class' => 'form-control', 'placeholder' => 'Description', 'size' => '20x2')) }}
                @if ($errors->has('description'))
                    <span class="help-block">
                        <strong>{{ $errors->first('description') }}</strong>
                    </span>
                @endif
            </div>

            {{ Form::submit('Submit', array('class' => 'btn btn-primary')) }}
        {{ Form::close() }}
    </div>
</div>