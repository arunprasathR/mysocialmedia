 @include('dashboard.uploadFiles')

<div class="{{ !empty($query) ? '' : ' ' }}">
    <button class="btn btn-success  -toggle btn-block" type="button" data-toggle=" ">Files
    <span class="caret"></span></button>
    <section class="{{ !empty($query) ? '' : ' -menu' }}">
        {{ Form::open(array('url' => '/searchFiles', 'method' => 'get', 'class' => 'form-horizontal' )) }}
            {{ csrf_field() }}

            <div class="form-group ">
                <div class="input-group col-md-8 col-md-offset-2">
                  {{ Form::text('query', null, array('class' => 'form-control', 'placeholder' => 'Search Files')) }}
                    <span class="input-group-btn">
                        <button class="btn btn-primary" type="submit">
                            <span class="glyphicon glyphicon-search"></span> Search
                        </button>
                    </span>
                </div>
                @if ($errors->has('query'))
                <span class="help-block">
                    <strong>{{ $errors->first('query') }}</strong>
                </span>
                @endif
            </div>
        {{ Form::close() }}

        @if(!empty($query))
            <h3> Search results of {{ $query }}   </h3>
        @endif
    
        <table class="table table-hover table-inverse table-striped">
            <thead>
                <tr>
                  <th>#</th>
                  <th>File Name</th>
                  <th>File Last Modified</th>
                  <th>View &amp; Download File</th>
                  <th>Delete File</th>
                </tr>
            </thead>
            <tbody>
                @foreach($files as $index => $file)
                    <tr>
                        <th scope="row">{{ $index + 1 }}</th>
                        <td>
                            <img src="{{ $file['iconLink'] }}"> {{ $file['name'] }}
                        </td>
                        <td>{{ Carbon\Carbon::parse($file['modifiedTime'])->format('d-m-Y h:i') }}</td>
                        <td>
                            <a href="{{ $file['webViewLink'] }}"  target="_blank">view</a>
                            @if(!empty($file->webContentLink))
                                <a href="{{ $file['webContentLink'] }}">download</a>
                            @endif
                        </td>
                        <td>
                            <button class="btn btn-danger btn-sm deleteDriveFile" id="{{ $file['id']}}">Delete</button>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </section>
</div>

