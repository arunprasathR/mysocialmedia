@extends('layouts.app')

@section('title', 'profile')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <section class="col-md-3"">

        <img src="{{ $user->avatar }}" style="width:170px; height:170px; border-radius:50%; margin-right:25px;">
      </section>

      <section class="col-md-7"">

        <h3>{{ $user->name }}'s Profile.</h3>

        <h4>
          <b>e-mail :</b> <a id="email" href="">{{ $user->email }} </a><i style="font-size:12px" id="updateEmail" class="fa fa-btn fa-btn-sm fa-pencil-square-o updateEmail" aria-hidden="true"></i>

          <input type="email"   style=" width:45%; display: none;" value="{{ $user->email }}"/>
          
          <span id="btn" style="display: none;"> 
            <button type="button" id="updateBtn" class="btn btn-success btn-xs">Update</button> 
            <button type="button" id="cancelBtn" class="btn btn-danger btn-xs">Cancel</button>
          </span>
        </h4>

        {{ Form::open(array('url' => '/profile', 'files' => true, 'method' => 'post')) }}

          {{ csrf_field() }}

          <div class="form-group">
            {{ Form::label('avatar', 'Update profile image', array('class' =>'control-label')) }}
            <div class="">
              {{ Form::file('avatar') }}
            </div>
          </div>

          <div class="form-group">
            {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}
          </div>
        {{ Form::close() }}
      </section>
    </div>
  </div>
</div>

<script src="assets/js/profile.js"></script>
@endsection
