@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">

            <h2>Welcome {!! $user->name!!} </h2>
            <h3>you are logged in</h3>
        </div>
    </div>
</div>
@endsection
