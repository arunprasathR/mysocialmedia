@extends('layouts.app')

@section('title', 'Reset Password')

@section('content')

<div class="container">
	<div class="row">
		<div class="col-sm-offset-2">
			<div class="panel panel-default">
				
				<div class="panel-body">
					@if (session('status'))
					<div class="alert alert-success">
						{{ session('status') }}
					</div>
					@endif

					{!! Form::open(array('url' => '/password/email', 'class' => 'form-horizontal')) !!}
					{{ csrf_field() }}
					
					<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">

						{!! Form::label('email', 'E-mail Address', array('class' => 'control-label col-md-4')) !!}
						<div class="col-md-6">
							{!! Form::text('email',null, array('class' => 'form-control')) !!}

							@if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
						</div>
					</div>

					<div class="form-group">
						<div class="col-md-6 col-md-offset-4">
							<button type="submit" class="btn btn-primary">
								<i class="fa fa-btn fa-envelope"></i> Send Password Reset Link
							</button>
						</div>
					</div>

					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection