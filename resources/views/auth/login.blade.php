@extends('layouts.app')

@section('title', 'login')

@section('content')
	<div class="container">

		<div class="row login">
			<div class="col-sm-offset-3 col-sm-6">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h2 class="panel-title">Login</h2>
					</div>

					<div class="panel-body">

						{!! Form::open(array('url' => '/login', 'class' => 'form-horizontal')) !!}
						<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">

							{!! Form::label('email', 'Email', array('class' => 'control-label col-sm-4')) !!}
							<div class="col-sm-7">
								{!! Form::text('email','', array('class' => 'form-control', 'value' => '')) !!}
								 @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">

							{!! Form::label('password', 'Password', array('class' => 'control-label col-sm-4')) !!}
							<div class="col-sm-7">
								{!! Form::password('password', array('class' => 'form-control', 'value' => '')) !!}
								 @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
							</div>
						</div>

						{!! Form::submit('Login', array('class' => 'btn btn-primary col-sm-offset-2')) !!}
						<a href="/password/reset"><span class="small">Forgot Password</span></a>
						{!! Form::close() !!}

						<hr>
						<h4 class="text-center">or</h4>
						<section class="col-md-12">
							<div class="col-sm-6 ">
								<a href="auth/google" class="btn btn-danger btn-block"> Login with Google </a>
							</div>

							<div class="col-sm-6">
								<a href="auth/facebook" class="btn btn-primary btn-block"> Login with Facebook </a>
							</div>
						</section>
						

						<section class="col-md-12" style="margin-top: 4%;">
							<div class="col-sm-6">
								<a href="auth/linkedIn" class="btn btn-success btn-block"> Login with LinkedIn </a>
							</div>

							<div class="col-sm-6">
								<a href="auth/twitter" class="btn btn-info btn-block"> Login with Twitter </a>
							</div>
						</section>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection