<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<title> @yield('title') &mdash; Apr</title>
	{!! Html::style('assets/css/login/login.css') !!}
		<style>
		
		</style>
	</head>

	<body>
		<div class="container">

			<div class="row login">
				<div class="col-sm-offset-3 col-sm-6">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h2 class="panel-title">@yield('heading')</h2>
						</div>

						<div class="panel-body">
							@yield('content')
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>