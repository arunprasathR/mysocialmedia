@extends('layouts.app')

@section('title', 'Pics')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">

            <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#picModal">Upload Pics</button>

            <div class="modal fade" id="picModal" role="dialog">
                <div class="modal-dialog">
                
                  <div class="modal-content" >
                    
                    <div class="modal-body" style="width: 80%; margin-left: 10%;">
                        {!! Form::open(array('url' => '/pic', 'class' => 'form-horizontal', 'files' => true )) !!}
                            <div class="form-group">
                                {!! Form::label('label', 'Label : ') !!}
                                {!! Form::text('label', null, array('class' => 'form-control', 'placeholder' => 'Label')) !!}
                                @if ($errors->has('label'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('label') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                {!! Form::label('description', 'Few Words : ') !!}
                                {!! Form::textarea('description', null, array('class' => 'form-control', 'placeholder' => 'Few words', 'size' => '30x3')) !!}
                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                {!! Form::label('pics', 'Choose your pics : ') !!}
                                {!! Form::file('pics[]',array('class'=>'form-control', 'multiple' => 'true')) !!}
                                @if ($errors->has('pics[]'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('pics[]') }}</strong>
                                    </span>
                                @endif
                            </div>

                            {!! Form::submit('Submit', array('class' => 'btn btn-primary')) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection