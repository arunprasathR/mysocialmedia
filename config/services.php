<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'google' => [
        'client_id' => '174672360819-evumis941sm60lsuoci1clfhultqkljs.apps.googleusercontent.com',
        'client_secret' => 'LoZNPTeDJuKCaBjS6pVQ7Ev-',
        'redirect' => 'http://laravel.app/auth/google/callback',
    ],

    'facebook' => [
        'client_id' => '115980228960339',
        'client_secret' => '1d6204de6f5cd93d491c917b4d2b388e',
        'redirect' => 'http://laravel.app/auth/facebook/callback',
    ],

    'linkedin' => [
        'client_id' => '86umpo7oldllif',
        'client_secret' => 'SOiTr9dmWqFcg3wC',
        'redirect' => 'http://laravel.app/auth/linkedIn/callback',
    ],

    'twitter' => [
        'client_id' => 'O24akrrWZyR0SDOk4SeQUmtLN',
        'client_secret' => 'EsmW3cg8qNwAhCL5nZWafZHxbae2vDwqdR7cXIUjZOC5piMPV7',
        'redirect' => 'http://laravel.app/auth/twitter/callback',
    ],

];
