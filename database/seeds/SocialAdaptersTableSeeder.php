<?php

use Illuminate\Database\Seeder;

class SocialAdaptersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('social_adapters')->truncate();

        DB::table('social_adapters')->insert([
        	[
        		'social_name' => 'Google'
        	],

        	[
        		'social_name' => 'Facebook'
        	],

        	[
        		'social_name' => 'LinkedIn'
        	],

        	[
        		'social_name' => 'Twitter'
        	],
        ]);
    }
}
