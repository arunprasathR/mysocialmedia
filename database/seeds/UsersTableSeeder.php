<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();

        // DB::table('users')->insert([
        // 	[
        // 		'name'  => 'arun',
	       //  	'email' => 'arun@arun.com',
	       //  	'password' => bcrypt('arun')
        // 	],
        // 	[
        // 		'name'	=>	'sri',
        // 		'email'	=>	'sri@sri.com',
        // 		'password'	=>	bcrypt('sri')
        // 	]
        // ]);
    }
}
