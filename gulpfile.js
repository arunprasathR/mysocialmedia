var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir.config.assetsPath = 'public/assets';
elixir.config.publicPath = elixir.config.assetsPath;

elixir.config.css.sass.pluginOptions.includePaths = [
	'node_modules/bootsrap-sass/assets/styleheets'
];

elixir(function(mix) {
	mix.styles([
        "font-awesome/css/font-awesome.min.css",
    ], 'public/assets/css/fonts', 'node_modules');

    mix.sass('login.scss','public/assets/css/login');
});
