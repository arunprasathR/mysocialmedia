<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pic extends Model
{
	protected $fillable = [
	'user_name', 'label', 'description', 'pic_name',
	];

	protected $hidden = [
	 'remember_token',
	];
}
