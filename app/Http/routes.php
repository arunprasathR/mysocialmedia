<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/', function () {
    return view('welcome');
    // return phpinfo();
});

Route::auth();

Route::get('/logout', 'Auth\AuthController@logout');

Route::get('auth/google', 'Auth\GoogleLoginController@redirectToGoogle');

Route::get('auth/google/callback', 'Auth\GoogleLoginController@handleGoogleCallback');

Route::get('auth/facebook', 'Auth\FacebookLoginController@redirectToFacebook');

Route::get('auth/facebook/callback', 'Auth\FacebookLoginController@handleFacebookCallback');

Route::get('auth/linkedIn', 'Auth\LinkedInLoginController@redirectToLinkedIn');

Route::get('auth/linkedIn/callback', 'Auth\LinkedInLoginController@handleLinkedInCallback');

Route::get('auth/twitter', 'Auth\TwitterLoginController@redirectToTwitter');

Route::get('auth/twitter/callback', 'Auth\TwitterLoginController@handleTwitterCallback');

Route::get('/home', 'HomeController@index');

Route::get('/dashboard', ['uses' => 'Dashboard\DashboardController@index' , 'as' => 'dashboard']);

Route::get('/searchFiles', 'Dashboard\DashboardController@searchFiles');

Route::post('/uploadFiles', 'Dashboard\DashboardController@uploadFiles');

Route::get('/delete', 'Dashboard\DashboardController@deleteFiles');

Route::get('/pic', 'PicsController@index');

Route::post('/pic', 'PicsController@uploadPics');

Route::get('/profile', 'ProfileController@profile');

Route::post('/profile', 'ProfileController@updateImage');

Route::post('/profile/updateEmail', 'ProfileController@updateEmail');

Route::get('/test', 'TestController@index');

Route::get('/result', ['uses' => 'TestController@result' , 'as' => 'result']);
