<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use Auth;
use Image;
use App\Pic;
use File;

class PicsController extends Controller
{
	/*
    |--------------------------------------------------------------------------
    | Picture Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the  upload of new images of the user, which would be displayed as | the profile pic for the user.
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
	public function __construct()
    {

        $this->middleware('auth');
    }


	public function index()
	{
		return view('pic', array('user' => Auth::user()));
	}

	public function uploadPics(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'label' => 'required|max:255',
			'description' => 'required|max:255',
			'pic.*' => 'required|mimes:jpeg,jpg,png',
			]);

		if (!$validator->fails()) {
			
			
			$user = Auth::user()->name;

			$path = public_path().'/assets/images/' . $user;

			File::makeDirectory($path, $mode = 0777, true, true);


			$uploadPics = $request->all();
			extract($uploadPics);

			foreach ($pics as $pic) {
				$imgPath = public_path(). '/assets/images/' . $user;
				$filename = $pic->getClientOriginalName();
				Image::make($pic)->resize(300, 300);

				$pic->move($imgPath , $filename);

				$new_pic = new Pic;
				$new_pic->user_name = Auth::user()->name;
				$new_pic->label = $label;
				$new_pic->description = $description;
				$new_pic->pic_name = $filename;
				$new_pic->save();
			}
		}
		

		if ($validator->fails()) {
			$validator_errors = $validator->errors()->all();
			return view('pic', array('user' => Auth::user(), 'errors' => $validator_errors));
		}

		return view('pic', array('user' => Auth::user()));
	}
}


















