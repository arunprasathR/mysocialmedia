<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\UserDetail;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Http\Requests;
use Image;
use Auth;
use Session;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */
    
    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    protected $imagePath = '/assets/images/userProfile/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:4|confirmed',
            'avatar' => 'required|mimes:jpeg,jpg,png',
        ]);
    }

    /**
     * Uploads a new image to the storage.
     *
     * @param  array  $data
     * @return filePath
     */
    
    public function uploadAvatar(){

        $request = Request::capture();

        if($request->hasFile('avatar')){
            $avatar = $request->file('avatar');
            $filename = $avatar->getClientOriginalName();
            $filePath = $this->imagePath . $filename;
            Image::make($avatar)->resize(300, 300)->save( public_path($filePath) );
            return $filePath;
        }

        return false;
    }

    protected function create(array $data)
    {

        $filePath = $this->uploadAvatar();

        $profile = UserDetail::create([
            'email' => $data['email'],
            'external_id' => null,
            'social_adapter' => null,
            'name' => $data['name'],
        ]);


        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'profile_id' => $profile->id,
            'social_integration' => 0,
            'avatar' => $filePath,
            'password' => bcrypt($data['password']),
        ]);
    }

    public function logout()
    {
        Auth::logout();
        Session::flush();
        return redirect($this->redirectTo);
    }
}
