<?php
namespace App\Http\Controllers\Auth;
use App\User;
use App\UserDetail;
use Auth;

use Socialite;

trait SocialLogin {

    public function socialTokenAppIDs()
    {
        return [1, 2];
    }
    
    public function handleSocialCallback($social, $social_id) 
    {
        try {
            $user = Socialite::driver($social)->user();
        } catch(Exception $e) {
            return redirect('auth/' . $social);
        }

        $authUser = $this->findOrCreate($user, $social_id);

        if($this->isTokenRequired($social_id)){
            $session = Session(); 
            $token = ['expires_in' => $user->expiresIn, 'access_token' => $user->token, 'refresh_token' => $user->refreshToken];
            $session->put($social . '.token', json_encode($token));
        }

        Auth::login($authUser, true);
        return $authUser;
    }

    public function isTokenRequired($social_id)
    {
        return in_array($social_id, $this->socialTokenAppIDs());
    }


    public function findOrCreate($user, $social_id) 
    {
    
    	$socialUser = UserDetail::where('external_id', $user->id)->value('id');
        if(!empty($socialUser)) {
            $authUser = User::where('profile_id', $socialUser)->first();
            if($authUser) {
                return $authUser;
            }
        }

        $profile = UserDetail::create([
            'email' => $user->email,
            'external_id' => $user->id,
            'social_adapter' => $social_id,
            'name' => $user->name,
        ]);

        return User::create([
            'name' => $user->name,
            'email' => $user->email,
            'profile_id' => $profile->id,
            'social_integration' =>1,
            'avatar' => $user->avatar_original,
    	]);
    }
}
