<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\User;
use App\Integration;
use Auth;
use App\Http\Controllers\Controller;
use Session;

use App\Http\Requests;

use Socialite;

class TwitterLoginController extends Controller
{   
    use SocialLogin;

    public function redirectToTwitter()
    {
       return Socialite::driver('twitter')->redirect();
    }

    public function handleTwitterCallback()
    {
        $social ='twitter';
        $social_id = 4;
        $this->handleSocialCallback($social, $social_id);
        return redirect('dashboard');
    }
}
