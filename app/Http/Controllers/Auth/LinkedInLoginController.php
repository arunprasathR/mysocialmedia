<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\User;
use App\Integration;
use Auth;
use App\Http\Controllers\Controller;
use Session;

use App\Http\Requests;

use Socialite;

class LinkedInLoginController extends Controller
{
    use SocialLogin;

    public function redirectToLinkedIn()
    {
       return Socialite::driver('linkedIn')->redirect();
    }

  public function handleLinkedInCallback()
    {
        $social ='linkedIn';
        $social_id = 3;
        $this->handleSocialCallback($social, $social_id);
        return redirect('dashboard');
    }
}
