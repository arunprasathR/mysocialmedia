<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\User;
use App\Integration;
use Auth;
use App\Http\Controllers\Controller;
use Session;

use App\Http\Requests;

use Socialite;

class FacebookLoginController extends Controller
{
    use SocialLogin;

    private $social ='facebook';
    private $socialId = 2;

    public function redirectToFacebook()
    {
       return Socialite::driver('facebook')->scopes(['email','user_posts'])->redirect();
    }

    public function handleFacebookCallback()
    {
        $user = $this->handleSocialCallback($this->social, $this->socialId);
        return redirect('dashboard');
    }
}
