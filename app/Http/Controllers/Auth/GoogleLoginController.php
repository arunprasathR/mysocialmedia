<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\User;
use App\Integration;
use Auth;
use App\Http\Controllers\Controller;
use Session;

use App\Http\Requests;

use Socialite;

class GoogleLoginController extends Controller
{
    use SocialLogin;

    public function redirectToGoogle()
    {
        return Socialite::driver('google')->scopes(['email','profile','https://www.googleapis.com/auth/drive'])->redirect();
    }

    public function handleGoogleCallback()
    {
    	try {
    	    $user = Socialite::driver('google')->user();
    	} catch(Exception $e) {
    		return redirect('auth/google');
    	}

        $social_id = '1';

        $authUser = $this->findOrCreate($user, $social_id);

    	Auth::login($authUser, true);

        //set access token for google drive
        $session = Session(); 
        $token = ['expires_in' => $user->expiresIn, 'access_token' => $user->token, 'refresh_token' => $user->refreshToken];
        $session->put('google.token', json_encode($token));


    	return redirect('dashboard');
    }
}
