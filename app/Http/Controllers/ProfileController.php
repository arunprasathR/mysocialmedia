<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;
use Image;

class ProfileController extends Controller
{

 /**
 * Create a new authentication controller instance.
 *
 * @return void
 */
  public function __construct()
  {

    $this->middleware('auth');
  }

  public function profile()
  {
    return view('profile', array('user' => Auth::user()));
  }

  public function updateImage(Request $request)
  {
    if($request->hasFile('avatar')){
      $avatar = $request->file('avatar');
      $filename = $avatar->getClientOriginalName();
      Image::make($avatar)->resize(300, 300)->save( public_path('/assets/images/userProfile/' . $filename ) );
      $filePath = '/assets/images/userProfile/' . $filename;

      $user = Auth::user();
      $user->avatar = $filePath;
      $user->save();
    }

    return view('profile', array('user' => Auth::user()));
  }

  public function updateEmail(Request $request)
  {
    $updateEmail = $request->all();
    extract($updateEmail);

    $user = Auth::user();
    $user->email = $email;
    $user->save();
  }
}
