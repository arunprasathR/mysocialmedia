<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Http\Requests;

use Auth;
use Validator;
use Session;
use App\User;
use App\UserDetail;
use App\Goog;
use Carbon\Carbon;

class DashboardController extends Controller
{
	use DriveFiles;
	use FacebookFeeds;

	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	* Return dashboard page 
	*
	* @param Request
	* @return View
	*
	**/
	public function index(Request $request)
	{

		$pageData = $this->googleIndex();
		$feeds = $this->feeds();

		return view('dashboard.dashboard', $pageData, array('user' => Auth::user(),'feeds' => $feeds));
	}

	public function googleIndex()
	{
		if (session()->has('google.token')) {
			$request = Request::capture();
			$message = null;
			$results = $request->all();
			extract($results);

			if ($request->has('query') || $request->hasFile('file')) {
				$results = $request->all();
				extract($results);

			}
			else {
				$files = $this->files();
				$query = null;
			}

			return compact('files', 'query', 'message');
		} else {
			return ['files' => false];
		}
	}

	public function searchFiles(Request $request) 
	{
		if ($request->has('query')) {
			$query =  $request->input('query');
			$results = $this->searchFile($query);
		}

		return redirect()->route('dashboard', $results);
	}

	public function uploadFiles(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'file' => 'required',
            'description' => 'required|max:255',
        ]);
        if (!$validator->fails()) {

            if ($request->hasFile('file')) {

                $file = $request->file('file');
                $description = $request->input('description');
                $results = $this->uploadFile($file, $description);
			}
			return redirect()->route('dashboard', ['message' => $results]);
        }

        if ($validator->fails()) {
            $validator_errors = $validator->errors();
            return view('dashboard.uploadFiles', ['user' => Auth::user(), 'errors' => $validator_errors]);
        }
    }

    public function deleteFiles(Request $request)
    {	
    	$deleteFile = $request->all();
    	extract($deleteFile);

    	$this->deleteFile($id);
    }

}
