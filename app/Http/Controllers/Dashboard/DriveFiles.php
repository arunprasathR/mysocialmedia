<?php
namespace App\Http\Controllers\Dashboard;
use Illuminate\Http\Request;

use App\Http\Requests;
use Validator;
use Auth;
use App\User;
use Session;
use App\UserDetail;
use Carbon\Carbon;

trait DriveFiles {

    public function client()
    {
        $client = new \Google_Client();
        $client->setClientId('174672360819-evumis941sm60lsuoci1clfhultqkljs.apps.googleusercontent.com');
        $client->setClientSecret('LoZNPTeDJuKCaBjS6pVQ7Ev');
        $client->setRedirectUri('http://laravel.app');
        $client->addScope(\Google_Service_Drive::DRIVE_READONLY);
        $client->setAccessType('offline');
        $client->setAccessToken(session('google.token'));

        return $client;
    }

    public function drive($client)
    {
        $drive = new \Google_Service_Drive($client);
        return $drive;
    }

    public function files()
    {
    	$client = $this->client();
    	$drive = $this->drive($client);
        
        if(session()->has('google.token')) {

            $result = [];

            $three_months_ago = Carbon::now()->subMonths(3)->toRfc3339String();

            try {
                $parameters = [
                    'q' => "viewedByMeTime >= '$three_months_ago' or modifiedTime >= '$three_months_ago'",
                    'orderBy' => 'modifiedTime desc',
                    'fields' => 'nextPageToken, files(id, name, modifiedTime, iconLink, webViewLink, webContentLink)',
                ];

                $result = $drive->files->listFiles($parameters);
                $files = $result->files;
            } catch (Exception $e) {
                
                return redirect('/files')->with('message',
                    [
                        'type' => 'error',
                        'text' => 'Something went wrong while trying to list the files'
                    ]
                );
            }

            return $files;
        }
    }

    public function searchFile($query)
    {
        $client = $this->client();
        $drive = $this->drive($client);
        
        $parameters = [
            'q' => "name contains '$query'",
            'fields' => 'files(id, name, modifiedTime, iconLink, webViewLink, webContentLink)',
        ];

        $result = $drive->files->listFiles($parameters);
        if($result){
            $files = $result->files;
        }

        return compact('files', 'query');
    }

    public function uploadFile($file, $description)
    {
        $client = $this->client();
        $drive = $this->drive($client);

        $mime_type = $file->getMimeType();
        $title = $file->getClientOriginalName();
        $descrip = $description;

        $drive_file = new \Google_Service_Drive_DriveFile();
        $drive_file->setName($title);
        $drive_file->setDescription($descrip);
        $drive_file->setMimeType($mime_type);

        try {
            $createdFile = $drive->files->create($drive_file, [
                'data' => $file,
                'mimeType' => $mime_type,
                'uploadType' => 'multipart'
            ]);

            $file_id = $createdFile->getId();

            $message ="Succesfully Uploaded";

            return $message;

        } catch (Exception $e) {

            $message = "An error occurred while trying to upload the file";

            return $message;
        }
    }

    public function deleteFile($id)
    {
        $client = $this->client();
        $drive = $this->drive($client);

        try {
            $drive->files->delete($id);
        } catch (Exception $e) {
            $message = "An error occurred while trying to upload the file";

            return false;
        }

        $message ="File was deleted";

        return true;

    }

}