<?php

namespace App\Http\Controllers\Dashboard;
use Illuminate\Http\Request;

use App\Http\Requests;
use Session;
use Facebook;
use Facebook\FacebookRequest;
use Facebook\GraphUser;

trait FacebookFeeds {

	public function feeds()
	{
		if(session()->has('facebook.token')) {
       
			$fb = new Facebook\Facebook([
				'app_id' => '115980228960339',
				'app_secret' => '1d6204de6f5cd93d491c917b4d2b388e',
			]);

			$accessToken = json_decode(session('facebook.token'));

			$response = $fb->get('/me/feed?fields=name,created_time,full_picture,message,link,story&limit=20', $accessToken->access_token);

			$feedRequest = $response->getBody();
			$fbFeeds = json_decode($feedRequest);

			$feeds = $fbFeeds->data;

			return $feeds;

		}
		else
		{
			return false;
		}
	}
}