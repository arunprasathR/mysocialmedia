<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDetail extends Model
{
    protected $fillable = [
    	'email', 'name', 'external_id', 'social_adapter',
    ];
}