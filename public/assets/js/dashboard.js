$(function(){

	//Deleting a File from drive using ajax
	
	$('.deleteDriveFile').on('click', function(){
		$deleteId = $(this).attr("id");

		if($deleteId)
		{
			console.log("hello");
			$.ajax({
				method: 'get',
				url: "/delete",
				data: {
					id: $deleteId,
					_token: $('input[name=_token]').val()
				},
			});
		}
        $(this).closest('tr').remove();
      	$('tbody tr').each(function() {
	        newId =  $(this).index() + 1;
	        $(this).children('th').first().html(newId);   
	    });
		
		console.log($deleteId);
	});
});