$(function(){
	
	
	$("#updateEmail").on('click',  function(){
		$("#email").hide();
		$("#updateEmail").hide();
		$('input[type=email]').toggle();
		$('#btn').toggle();
	});

	$("h4").on('click', '#updateBtn', function(){
		$newEmail = $('input[type=email]').val();
		$.ajax({
			method: 'post',
			url: "/profile/updateEmail",
			data: {
				email: $newEmail,
				_token: $('input[name=_token]').val()
			},
			success: function (data) {
				$("#email").show();
				$("#updateEmail").show();
				$('input[type=email]').toggle();
				$('#btn').toggle();
				$('#email').text($newEmail);
				console.log("hello");
			},
		});
	});

	$("h4").on('click', '#cancelBtn', function(){
		$("#email").show();
		$("#updateEmail").show();
		$('input[type=email]').toggle();
		$('#btn').toggle();
	});
});